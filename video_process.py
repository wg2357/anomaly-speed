'''
video_process contains the functions required for converting video files into
numpy arrays, tiling the video, processing tiles, and outputting a new processed
video for the user to see the results of anomaly detection. 
'''
import numpy as np
import cv2 as cv
import math as m
import iforest as fo

class Video:
    def __init__(self, in_file, in_frames=None):
        self.data   = []
        self.cap    = cv.VideoCapture(in_file)
        self.height = int(self.cap.get(4))
        self.width  = int(self.cap.get(3))
        self.fps    = int(self.cap.get(5))
        self.tiling = None

        if in_frames is None:
            while self.cap.isOpened():
                ret, frame = self.cap.read()
                if ret:
                    self.data.append(frame)
                else:
                    break
            self.cap.release()
            self.data = np.array(self.data)
        else:
            for i in range(0, in_frames):
                ret, frame = self.cap.read()
                if ret:
                    self.data.append(frame)
                else:
                    break
            self.cap.release()
            self.data = np.array(self.data)

        self.nframes = len(self.data)

    def view_tiling(self, tiling):
        '''
        view_tiling takes in the list of 3 elements from the user input
        and creates a window displaying the tiling entered. This is used
        in main() for confirming the tiling
        '''
        rec_bounds = self.tile_box_bounds(tiling)
        for frame in self.data:
            for rec in rec_bounds:
                cv.rectangle(frame, rec[0], rec[1], (0,255,0), 1)
            cv.imshow('frame', frame)
            if cv.waitKey(0) & 0xFF == ord('q'):
                break
        cv.destroyAllWindows()

    def write_video(self, filename, tiling, anoms, velocities):
        '''
        input: output filename
               np.array of 0/1 anomalies
               velocities and frames tuple
        output: video with anomalies highlighted and speeds written
        '''
        tile_boxes = self.tile_box_bounds(tiling)
        fourcc = cv.VideoWriter_fourcc(*'XVID')
        out = cv.VideoWriter(filename,fourcc, self.fps, (self.width, self.height))
        note_coord = [(i * int(self.width/4), int(self.height/4)) for i in range(3)] \
                      + [(i * int(self.width/4), 3*int(self.height/4)) for i in range(3)]

        for i in range(0, self.nframes):
            frame = self.data[i]
            for t in range(0, len(tile_boxes)):
                if anoms[t][i] == 1:
                    cv.rectangle(frame, tile_boxes[t][0], tile_boxes[t][1],
                                 (0,255,0), 1)
                else:
                    pass

            counter = 0
            for v in velocities:
                if i in range(v[1][0], v[1][1]):
                    counter += 1
                    cv.putText(frame, "{} mph".format(v[0]),
                               note_coord[counter-1], cv.FONT_HERSHEY_SIMPLEX,
                               0.5, (0, 0, 255), 1)

                else:
                    pass

            out.write(frame)

        out.release()

    def extract_tiles(self):
        '''
        extract_tiles takes in the tiling list and returns a list where each
        element is a np.array representing the whole video for that tile
        '''
        row_top, tile_height, tile_width = self.tiling
        x_bounds = list(range(0, self.width, tile_width))
        return [self.data[:, row_top:row_top+tile_height, x:x+tile_width, :]
                for x in x_bounds]

    def tile_box_bounds(self, tiling):
        '''
        self_draw_tile_bounds takes in the tiling list and produces a tuple
        (upper left, lower right) for easier drawing in the write_file function
        '''
        row_top, tile_height, tile_width = tiling
        x_bounds   = [i for i in range(0, self.width, tile_width)]
        tleft      = [(x, row_top) for x in x_bounds]
        bright     = [(x+tile_width, row_top+tile_height) for x in x_bounds]
        return list(zip(tleft, bright))

def summary(tile):
    def summary_helper(frame):
        collapsed = np.vstack(frame)
        t_data    = np.concatenate([collapsed.mean(axis=0), collapsed.var(axis=0)])
        return t_data
    return np.array([summary_helper(f) for f in tile])

def auto_tile(in_file, side_length):
    '''
    auto_tile uses the first 1000 frames of the video to try to automatically determine
    where to put the tile row. the best row is chosen by adding the sum of anomaly 
    scores for each tile. 
    '''
    training_frames = Video(in_file, in_frames=1000)
    y_bounds = list(range(0, training_frames.height, m.floor(side_length/3)))
    training_tiles = [training_frames.data[:,
                                           y:y+side_length,
                                           m.floor((training_frames.width - side_length)/2):
                                           m.floor((training_frames.width + side_length)/2),
                                           :]
                      for y in y_bounds]
    training_scores = np.array([fo.iForest(summary(t), 256, 100).anomaly_scores()
                                for t in training_tiles])
    top_scoring = np.argsort(training_scores.sum(axis=1))
    return [[y_bounds[i], side_length, side_length] for i in top_scoring]

