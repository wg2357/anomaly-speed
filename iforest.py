import numpy as np
import math as m
import random as r

'''
This algorithim is adapted from Ting, Liu, and Zhou (2011)
'''

class iForest:
    def __init__(self, X, t, s):
        '''
        A forest is grown for each tile to detect anomalies.
        Input: X: the processed data for one tile. class is numpy array, 
                  shape is (frames, attributes)
               t: a parameter, the number of trees to grow.
               s: a parameter, the size of the subsample
        Output: the forest
        self.limit chosen based on the paper
        self.penalties added to improve efficiency in calculating path length
            penalties.

        '''
        self.data        = X
        self.ntrees      = t
        self.sample_size = s
        self.limit       = m.ceil(m.log2(s))
        self.forest      = []
        self.n           = len(X)
        self.penalties   = [0, 0, 1] + [(2 * (m.log(i-1) + 0.5772156649) -
                                        (2 * (i-1) / self.sample_size))
                                        for i in range(3, self.sample_size+1)
                                       ]
        self.cn          = (2 * (m.log(self.sample_size - 1) + 0.5772156649) - 
                            (2 * (self.sample_size - 1) / self.sample_size)
                           )

        for i in range(s): 
            subset_index = r.sample(range(self.n), s)
            data_subset  = X[subset_index]
            self.forest.append(iTree(data_subset, 0))

    def anomaly_scores(self):
        '''
        Input: the forest
        Output: anomaly scores for every frame in the tile. 
        '''
        def anomaly_scores_helper(x):
            lengths = [path_length(x, T, self.limit)
                       for T in self.forest]
            penalized_lengths = np.array([i[0] + self.penalties[i[1]] for i in lengths])
            return (2 ** (-np.mean(penalized_lengths) / self.cn))

        return np.array([anomaly_scores_helper(d) for d in self.data])

class iTree:
    '''
    Input: X: a subset of the data from an individual tile. This will be
                        a list of random tile statistics.
           e: current tree height
    '''
    def __init__(self, X, e):
        self.length    = e
        self.size      = len(X)

        if self.size <= 2:
            self.left  = None
            self.right = None

        else:
            # select random attribute
            self.splitAtt = np.random.randint(len(X[0]))
            # range of possible values for the attribute
            self.attMax   = np.max(X[:,self.splitAtt])
            self.attMin   = np.min(X[:,self.splitAtt])

            if self.attMax == self.attMin:
                self.left = None
                self.right = None

            else:
                # choose random value to split at
                self.splitVal = np.random.uniform(low=self.attMin,
                                                  high=self.attMax, 
                                                  size=1
                                                  )[0]
                # subset data
                XL = X[X[:,self.splitAtt] < self.splitVal]
                XR = X[X[:,self.splitAtt] >= self.splitVal]
                self.left  = iTree(XL, self.length + 1)
                self.right = iTree(XR, self.length + 1)

def path_length(x, T, l):
    '''
    Traverse the tree using splitvals and use the terminal node's properties.
    The tree terminates when there's no left  attribute.
    Input: x: a tile summary vector 
           T: an instance of an iTree
           l: limit of path length to check
    Output: Integer path length, integer size of tree below that node as a tuple
    '''
    while (not (T.left is None)) and T.length < l:
        if x[T.splitAtt] < T.splitVal:
            T = T.left

        else:
            T = T.right

    return (T.length, T.size)
