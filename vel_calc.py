import numpy as np
import intervaltree as t

def smooth_scores(scores, threshold=0.5):
    '''
    Input: anomaly scores: shape [tiles, frames]
           threshold: default 0.5. If the score is above the threshold, it 
                      gets stored as 1 as a confirmed anomaly.
    Output: List, same shape as input, of zeroes and ones. This gets passed
            to write_video to draw the boxes on the proper frames. This is 
            also passed to anomaly_intervals.
    '''
    return np.where(scores>threshold, 1, 0)

def anomaly_intervals(scores):
    '''
    Input: 0/1 anomalies with shape [tiles, frames]
    Output: List of intervals in which anomalies are detected. [tiles, [interval]]
    '''
    diffs = np.diff(scores)
    def intervals_helper(x):
        intervals = []

        for i in range(len(x)):

            if x[i] == 1:
                intervals.append(i + 1)

            elif x[i] == -1:
                intervals.append(i)

            else:
                pass

        if len(intervals) % 2 == 1:
            intervals.append(len(x) + 1)

        else:
            pass

        return [[intervals[i], intervals[i+1]] for i in range(0,len(intervals),2)]

    return [intervals_helper(d) for d in diffs]

def anomaly_interval_tree(intervals):
    '''
    anomaly_interval_tree starts interval trees in the first tile and finds overlapping
    intervals. These are individual anomalies tracked across the tiles.
    input: intervals from anomaly_intervals
    output: interval tree
    '''
    anomalies = [t.IntervalTree.from_tuples([i]) for i in intervals[0]]

    # list flattening list comprehension taken from
    # https://stackoverflow.com/questions/11264684/flatten-list-of-lists
    all_others = [val for sublist in intervals[1:] for val in sublist]
    for i in all_others:
        for j in anomalies:
            if j.overlaps(i[0], i[1]):
                j.addi(i[0], i[1])

    return anomalies

def velocity(int_tree_anom, fps, frame_width_feet, ntiles):
    '''
    input: list of interval trees for eath anomaly and video parameters for conversion
           to miles per hour.
    output: tuples (velocity, [start frame, end frame]) to be passed to video_write
    '''
    yx = [[],[]]
    an_tree = sorted(int_tree_anom)
    for i in range(len(an_tree)):
        yx[0].append(np.repeat(i, an_tree[i].length()))
        yx[1].append(np.arange(an_tree[i].begin, 
                               an_tree[i].end))

    y = np.concatenate(yx[0])
    x_temp = np.concatenate(yx[1])
    x = np.vstack([np.ones(len(x_temp)), x_temp]).T
    mph = convert_to_vel(np.linalg.lstsq(x, y, rcond=None)[0][1], 
                         fps, frame_width_feet, ntiles)

    return (mph, [x_temp.min(), x_temp.max()])

def convert_to_vel(tpf, fps, frame_width_feet, ntiles):
    '''
    convert_to_vel is a convenient conversion from tiles per frame to miles per hour
    '''
    return round((tpf * fps * 3600 * (frame_width_feet / ntiles))/5280, 0)
