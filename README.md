Detecting Speed of Anomalies from Video
===

Statistical Computing Semester Project, Fall 2018

Wren

Background
---

The main goal of this program is to estimate the speed of an object moving across the frame of a video taken by a static camera. Using a particular implementation of a random forest (an isolation forest, as described in "Isolation-Based Anomaly Detection", Liu, Ting, and Zhou 2011), anomalies in the frame are detected and tracked across the frame. Speed is estimated using linear regression. Example output is included in `output.avi`, which was produced by running the program on `traffic-short.mp4`. A clip from this example is included in output-sample.gif. 

Outline
---

Anomalies are detected within "tiles" of the video frames, a box of pixels specified by the user. When the means and variances of the RGB values within these tiles change substantially from the baseline, the frame is flagged as having an anomaly. 

The video is first converted to a numpy array of RGB values using OpenCV. The user can manually specify the location and dimension of the tiles, or the program can attempt to automatically locate the correct location at which to place the tiles. A single frame is displayed with the specified tiles overlaid for confirmation by the user.

Tiles are converted to 6 dimensional summaries of their pixel values: the mean and variance of red, green, and blue pixel values. For each tile, there is therefore a data set of 6 variables and observations equal to the number of frames. The data set is split in two randomly: first randomly selecting a feature, then randomly selecting a value between the minimum and maximum on which to split. 

After creating many binary trees, each observation/frame is traced through tree. The path lengths are averaged, and an anomaly score is calculated. Scores above a certain threshold are considered anomalous. 

The speed at which an object traverses the tiles is first calculated as tiles per frame using linear regression. Based on the physical distance between the edges of the frame, entered by the user, speed is converted to miles per hour. 

Finally, OpenCV is used to output a video in which tiles are highlighted when anomalies are detected and speed is displayed at the top of the frame.

Running the Program
---

The code can be run using the following command:

`python3 anomaly_speed.py [VIDEO]`

Where `[VIDEO]` is a file path to an appropriate video.
