import pytest
from video_process import *
from vel_calc import *
from iforest import *
import numpy as np
import cv2 as cv
import math as m
import intervaltree as t

'''
vid2.mp4 is a clip of the test video from bigml.com. The particular
features being tested were double checked by opening the video clip with
VLC media player and checking the clip information.
'''
x = Video("vid2.mp4")

def test_video_processing():
    assert isinstance(x, Video)
    assert x.fps     == 30
    assert x.height  == 112
    assert x.width   == 384
    assert x.nframes == 331

def test_iforest():
    test_summary = np.array([[0, 0, 0, 1, 1, 1],
                             [0, 0, 0, 1, 1, 1],
                             [0, 0, 0, 1, 1, 1],
                             [0, 0, 0, 1, 1, 1],
                             [0, 0, 0, 1, 1, 1],
                             [0, 0, 0, 1, 1, 1],
                             [0, 0, 0, 1, 1, 1],
                             [0, 0, 0, 1, 1, 1],
                             [0, 0, 0, 1, 1, 1],
                             [0, 0, 0, 1, 1, 1],
                             [10, 30, 15, 16, 32, 86]])

    test_tree = iTree(test_summary, 0)
    '''
    Test tree structure, ensure tree terminates at correct time and sizes are
    correct.
    '''
    assert isinstance(test_tree, iTree)
    assert test_tree.left.size == 10
    assert test_tree.right.size == 1
    assert test_tree.left.left is None
    assert test_tree.left.right is None
    assert test_tree.right.left is None
    assert test_tree.right.right is None
    '''
    Test to ensure path length score returns 1 if the point is found, a 
    penalty if the size of the leaf is large, and a penalty if the 
    limit of the path checked is hit
    '''
    assert path_length(test_summary[0], test_tree, 10)[0] == 1
    assert path_length(test_summary[0], test_tree, 10)[1] == 10
    assert path_length(test_summary[-1], test_tree, 10)[0] == 1
    assert path_length(test_summary[-1], test_tree, 10)[1] == 1

def test_iforest_random_data():
    random_data = np.reshape(np.random.randint(0, 1024, 600), [100, 6])
    rforest = iForest(random_data, 50, 50)
    scores = rforest.anomaly_scores()
    '''
    Test bounds of anomaly scores
    '''
    assert np.any(scores < 0) == False
    assert np.any(scores > 1) == False
    
def test_smooth_scores():
    random_scores = np.random.random(100)
    assert len(np.where(random_scores > 0.5)[0]) == sum(smooth_scores(random_scores))
    assert np.all(smooth_scores(random_scores)) in [0,1]

test_intervals = np.array([[0., 1., 1., 0., 0., 0., 1., 1., 1., 0.],
                           [0., 0., 1., 1., 1., 0., 0., 1., 1., 1.],
                           [0., 0., 0., 0., 1., 1., 0., 0., 1., 1.],
                           [0., 0., 0., 0., 0., 1., 1., 0., 0., 0.],
                           [0., 0., 0., 0., 0., 0., 1., 1., 0., 0.],
                           [0., 0., 0., 0., 0., 0., 0., 1., 1., 1.]])

def test_anomaly_intervals():
    random_anoms = np.reshape(np.random.randint(0,2,500), [10,50])
    rand_intervals = anomaly_intervals(random_anoms)
    for i in rand_intervals:
        for j in i:
            # Test that the intervals are length 2 and the start is less than the end
            assert len(j) == 2
            assert np.diff(j) >= 0

    assert anomaly_intervals(test_intervals) == [[[1,2],[6,8]],
                                                 [[2,4],[7,10]],
                                                 [[4,5],[8,10]],
                                                 [[5,6]],
                                                 [[6,7]],
                                                 [[7,10]]]

test_int_tree = anomaly_interval_tree(anomaly_intervals(test_intervals))

def test_anomaly_interval_tree():
    assert len(test_int_tree) == 2
    assert type(test_int_tree[0]) == t.IntervalTree

def test_velocity():
    vtest = [velocity(i, 1, 1, 1) for i in test_int_tree]
    assert len(vtest) == 2
    assert m.isclose(0, vtest[0][0])
    assert vtest[1][1] == [6, 9]
