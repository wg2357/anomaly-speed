import argparse
import numpy as np
import cv2 as cv
import math as m
import video_process as v
import iforest as fo
import vel_calc as vel 
import intervaltree as t

def main():
    '''
    main() provides the program flow for an interactive experience
    '''
    parser = argparse.ArgumentParser(description='Anomaly Detection')
    parser.add_argument('file', metavar='f', type=str)
    args = parser.parse_args()

    print("<<<Anomaly detection in: {}>>>".format(args.file), "\n")
    print("The tiling can be created automatically or manually.")

    tiling_args = tiling_loop(args.file)

    frame_dist = frame()

    forest_param = forest()

    print("Please wait...")

    # load the full video
    video = v.Video(args.file)
    video.tiling = tiling_args

    while True:
        # Calculate a list of scores. 
        scores = np.array([fo.iForest(v.summary(t), *forest_param[1:3]).anomaly_scores() 
                          for t in video.extract_tiles()])
        intervals = vel.anomaly_intervals(vel.smooth_scores(scores, forest_param[0]))

        try:
            # intervals can sometimes end up being length 0, so a try/catch is needed
            velocities = [vel.velocity(i, video.fps, frame_dist, video.width/tiling_args[2]) 
                          for i in vel.anomaly_interval_tree(intervals)]
            break

        except ValueError:
            print("Error in creating interval trees. Trying again...")
            pass

    
    video.write_video('output.avi', 
                      video.tiling, 
                      vel.smooth_scores(scores, forest_param[0]),
                      velocities)

    print("'output.avi' created")


affirm = ["yes", "y", "yiss"]
negate = ["no", "n", "naw"]

def grab_frame(in_file):
    return v.Video(in_file, in_frames=1)

def tiling_loop(in_file):
    '''
    tiling_loop allows the user to choose which tiling method they want.
    It then displays the tiling using one frame of the video. If the tiling is 
    acceptable, the function returns arguments to get passed to the tiling
    method of the video object. If the tiling is unacceptable, it loops back
    on itself til the user finds an accepable tiling.
    '''
    while True:
        print("Would you like to manually specify the tiling? y/n")
        man_or_auto = input().lower()

        if man_or_auto in affirm: 
            try:
                print("Please specify the upper bound of the tiles")
                row_top = int(input())
                print("Please specify the height of the tiles")
                tile_height = int(input())
                print("Please specify the width of the tiles")
                tile_width = int(input())
            except ValueError:
                print("Please input an integer")

            tiling_confirmation(in_file, [row_top, tile_height, tile_width])
            return [row_top, tile_height, tile_width]

        elif man_or_auto in negate: 
            try:
                print("Please specify the side length of the tiles")
                side_length = int(input())
            except ValueError:
                print("Please input an integer")

            print("Please wait...")
            tiling_options = v.auto_tile(in_file, side_length)

            print("Please select one of the following three tilings")
            print("Option 1")
            grab_frame(in_file).view_tiling(tiling_options[0])
            print("Option 2")
            grab_frame(in_file).view_tiling(tiling_options[1])
            print("Option 3")
            grab_frame(in_file).view_tiling(tiling_options[2])
            print("What is your selection?")
            selection = int(input())
            if selection in [1, 2, 3]:
                return tiling_options[selection - 1]
            else:
                tiling_loop(in_file)

        else:
            print("I didn't understand")

def tiling_confirmation(in_file, tiling):
    grab_frame(in_file).view_tiling(tiling)
    while True:
        print("Is this the tiling you want?")
        good_tiling = input().lower()
        if good_tiling in affirm:
            print("nice")
            return None
        elif good_tiling in negate:
            print("aww")
            tiling_loop(in_file)
        else:
            print("I didn't understand")
def frame():
    while True:
        print("What is the distance (in feet) from one end of the video frame",
               "to the other?")
        try:
            frame_distance = float(input())
            return frame_distance
        except ValueError:
            print("Please input a number")

def forest():
    while True:
        print("\nThe default threshold score for a confirmed anomaly is 0.5",
              "The default sample size for each tree instance is 256",
              "The default number of trees per forest is 100",
              "Are these defaults acceptable?", sep="\n")
        default_check = input().lower() 
        if default_check in affirm: 
            return [0.5, 256, 100]
        elif default_check in negate: 
            while True:
                try:
                    print("Please provide a threshold between 0 and 1")
                    th = float(input())
                    if (th <= 0) or (th >=1): 
                        print("Number not between 0 and 1")
                        break
                    print("Please provide a sample size for the tree instances")
                    size = int(input())
                    print("Please provide the number of trees per forest")
                    trees = int(input())
                    return [th, size, trees]
                except ValueError:
                    print("Please input a number")
        else:
            print("I didn't understand")
main()
